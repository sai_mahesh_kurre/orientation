**DOCKER**:-  
* Docker helps developers build lightweight and portable software containers that simplify application development, testing, and deployment.     
* Docker is a software platform for building applications based on containers — small and lightweight execution environments that make shared use of the operating system kernel but otherwise run in isolation from one another.     
* While containers as a concept have been around for some time, Docker, an open source project launched in 2013, helped popularize the technology, and has helped drive the trend towards containerization and microservices in software development that has come to be known as cloud-native development

![](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)

**What are containers?**:-  
* One of the goals of modern software development is to keep applications on the same host or cluster isolated from one another so they don’t unduly interfere with each other’s operation or maintenance. This can be difficult, thanks to the packages, libraries, and other software components required for them to run.  
* One solution to this problem has been virtual machines, which keep applications on the same hardware entirely separate, and reduce conflicts among software components and competition for hardware resources to a minimum.  
* But virtual machines are bulky—each requires its own OS, so is typically gigabytes in size—and difficult to maintain and upgrade.

Containers, by contrast, isolate applications’ execution environments from one another, but share the underlying OS kernel. They’re typically measured in megabytes, use far fewer resources than VMs, and start up almost immediately. They can be packed far more densely on the same hardware and spun up and down en masse with far less effort and overhead. Containers provide a highly efficient and highly granular mechanism for combining software components into the kinds of application and service stacks needed in a modern enterprise, and for keeping those software components updated and maintained.

![](https://images.idgesg.net/images/article/2017/06/virtualmachines-vs-containers-100727624-large.jpg)
   
   
How the virtualization and container infrastructure stacks stack up. 

To understand how Docker works, let’s take a look at some of the components you would use to create Docker-containerized applications.

![](https://docs.docker.com/engine/images/engine-components-flow.png)

**Dockerfile**  
* Each Docker container starts with a Dockerfile. A Dockerfile is a text file written in an easy-to-understand syntax that includes the instructions to build a Docker image (more on that in a moment). A Dockerfile specifies the operating system that will underlie the container, along with the languages, environmental variables, file locations, network ports, and other components it needs—and, of course, what the container will actually be doing once we run it.  
* Paige Niedringhaus over at ITNext has a good breakdown of the syntax of a Dockerfile.

**Docker image**  
* Once you have your Dockerfile written, you invoke the Docker build utility to create an image based on that Dockerfile. Whereas the Dockerfile is the set of instructions that tells build how to make the image, a Docker image is a portable file containing the specifications for which software components the container will run.  

**Docker advantages**  
* Docker containers provide a way to build enterprise and line-of-business applications that are easier to assemble, maintain, and move around than their conventional counterparts.
* Also, saves lot of memory when compared to virtual machines.

![](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/10/virtual-machine-vs-docker-example-what-is-docker-container-edureka-1.png)]

**Docker containers enable portability**  
* A Docker container runs on any machine that supports the container’s runtime environment. Applications don’t have to be tied to the host operating system, so both the application environment and the underlying operating environment can be kept clean and minimal.  
* For instance, a MySQL for Linux container will run on most any Linux system that supports containers. All of the dependencies for the app are typically delivered in the same container.  
* Container-based apps can be moved easily from on-prem systems to cloud environments or from developers’ laptops to servers, as long as the target system supports Docker and any of the third-party tools that might be in use with it, such as Kubernetes (see “Docker containers ease orchestration and scaling,” below).  

![](https://devopedia.org/images/article/101/8323.1565281088.png)

**Docker containers enable composability**  
* Most business applications consist of several separate components organized into a stack—a web server, a database, an in-memory cache. Containers make it possible to compose these pieces into a functional unit with easily changeable parts. Each piece is provided by a different container and can be maintained, updated, swapped out, and modified independently of the others.  
* This is essentially the microservices model of application design. By dividing application functionality into separate, self-contained services, the microservices model offers an antidote to slow traditional development processes and inflexible monolithic apps. Lightweight and portable containers make it easier to build and maintain microservices-based applications.

**DOCKER COMMANDS**
* Below image is a list of docker commands which are helpful at the time of docker usage

![](https://phoenixnap.com/kb/wp-content/uploads/2019/11/docker-commands-cheatsheet-webpage.jpg)