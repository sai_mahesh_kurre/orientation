**GIT**:-  
* Git is currently the most popular implementation of a distributed version control system.  
* Git originates from the Linux kernel development and was founded in 2005 by Linus Torvalds. Nowadays it is used by many popular open source projects, e.g., the Android or the Eclipse developer teams, as well as many commercial organizations.  
* The core of Git was originally written in the programming language C, but Git has also been re-implemented in other languages,  e.g., Java, Ruby and Python.

**GIT COMMANDS**
![](https://res.cloudinary.com/practicaldev/image/fetch/s--ShHSfi-a--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://cl.ly/1N2U2i2Z2C16/Image%25202018-04-11%2520at%252012.47.23%2520PM.png)

**$git config**
* You can use it to configure the author's name, email address, file formats and many more to be used with your commits

**$git init**
* Using this command you make it sure that your git repository is initialized and creates the initial .git directory in a new or in an existing project.

**$git clone <path>**
* This creates a working copy of a Git repository from a remote source to your local repository. This is the first command you want to use when you are cloning a Git repository.

**$git add <file_name>**
* Add one or more files in your working directory to your index.

**$git commit**
* Take all your changes written in the index to the HEAD branch with a -m message.

**$git status**
* It shows you the status difference between an index and working directory files. Lists the files you've changed, untracked because they are only in your working directory and staged since they are ready to be committed.

**$git remote**
* Shows all the remote versions of your repository.

**$git checkout <branch_name>**
* You can switch from an existing branch to another one or create a new branch and switch to it git checkout -b <branch_name>.

**$git branch**
* With this, you can simply list all existing branches, including remote branches by using -a or create a new branch if a branch name is provided.

**$git push**
* Pushes all changes to the remote repository.

**$git pull**
* Fetch and merge your changes in the remote repository to your working directory.

**$git merge <branch_name>**
* Merges one or more branches into your active branch and if there are no conflicts it will automatically create a new commit.

**$git diff**
* Show changes between your working tree and the index, between two branches, or changes between two files on disk.

**$git reset**
* Reset your index and working directory to the state of your last commit.

**$git revert**
* Revert works in a very similar way to $git reset, but instead of resetting it will create a new commit that reverses everything introduced by the accidental commit.

**$git tag**
* You can use tagging to mark a significant change you made, such as a release.

**$git log**
* It shows a listing of commits on a branch with corresponding details.

**GIT REPOSITORIES**:-  
* A Git repository contains the history of a collection of files starting from a certain directory. The process of copying an existing Git repository via the Git tooling is called cloning. After cloning a repository the user has the complete repository with its history on his local machine. Of course, Git also supports the creation of new repositories.  
* If you want to delete a Git repository, you can simply delete the folder which contains the repository.  
* If you clone a Git repository, by default, Git assumes that you want to work in this repository as a user. Git also supports the creation of repositories targeting the usage on a server.  
* Bare repositories are supposed to be used on a server for sharing changes coming from different developers. Such repositories do not allow the user to modify locally files and to create new versions for the repository based on these modifications.  
* on-bare repositories target the user. They allow you to create new changes through modification of files and to create new versions in the repository. This is the default type which is created if you do not specify any parameter during the clone operation.  
* A local non-bare Git repository is typically called local repository

![](https://www.git-tower.com/learn/media/pages/git/ebook/en/command-line/remote-repositories/introduction/-1045933932-1597909835/basic-remote-workflow.png)

**Required Vocabulary**  
**Repository:**  
* Often called as a repo.  
* A repository is the collection of files and folders (code files) that you’re using git to track. It’s the big box you and your team throw your code into.

**GitLab:**  
* The 2nd most popular remote storage solution for git repos.

**Commit:**  
* Think of this as saving your work. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment.   
* The commit will only exist on your local machine until it is pushed to a remote repository.

**Push:**  
* Pushing is essentially syncing your commits to GitLab.

**Branch:**  
* You can think of your git repo as a tree. The trunk of the tree, the main software, is called the Master Branch. The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.

**Merge:**  
* When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase, it will get merged into the master branch. Merging is just what it sounds like: integrating two branches together.

**Clone:**  
* Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.

**Fork:**  
* Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

**THE BENEFITS OF GIT**  

Git is an easy-to-use software that helps you enhance your development system in numerous ways:    
* Productivity improvement  
* Code-quality improvement  
* Freedom to not worry about versions  
* The option of experimentation  
* Teamwork  
* Tracking changes  
* Undoing mistakes  
* Progress overview  
* Working offline on your local server  
* Being carefree about data loss

**GIT PROCESS**
![Git development](https://kodingnotes.files.wordpress.com/2013/12/git.png)


![Git branching model](https://nvie.com/img/centr-decentr@2x.png)


**HOW GIT CAN IMPROVE YOUR DEVELOPMENT PROCESS:**  
Every project is different from another because each website development process demands a different approach. Some teams may be used to one workflow, whereas others to something completely unseen. However, there are some proven workflows you can implement in your projects:

* **Basic workflow** – Git developers today like using this workflow for simple websites because right from the start they copy/clone the central repository and work locally on their piece of code, and only then do they apply it to the central repo for other developers to use.
* **Feature branch** – Git introduced branches to improve the code and this core feature is what makes it so special. Feature branch implies two (or more) people working on different features separately and then joining each branch with the master branch independently from the second (or third or any other) branch, without ruining the code.
* **Gitflow** – this workflow allows for the development of complex projects with two parallel branches: (1) master branch, which was tested and is ready to be released live at any moment, and (2) develop branch, i.e. the branch with features where all tests are performed. The latter can be merged with the former only when all bugs have been fixed and everything tested.
